# thylove-index

#### 介绍
vue3+ts + pinia项目

#### 软件架构
软件架构说明


#### 安装教程

1. 安装依赖
   1. 代码规范依赖
      
       * ```bash
         # eslint
         cnpm install eslint -D
         
         # 生成配置文件
         npx eslint --init
         ```
         
         ![image-20240401102308874](C:\Users\93787\AppData\Roaming\Typora\typora-user-images\image-20240401102308874.png)
         
       * ```js
         module.exports = {
             "env": {
                 "browser": true,
                 "es2021": true,
             },
             // 规则继承
             "extends": [
                 // 全部规则默认是关闭的，这个配置开启推荐规则。
                 // 比如函数不能重名，对象不能出现重复key
                 "eslint:recommended",
                 // ts语法规则
                 "plugin:@typescript-eslint/recommended",
                 // vue3语法规则
                 "plugin:vue/vue3-essential"
             ],
             // 要为特定类型的文件指定处理器
             "overrides": [
                 // {
                 //     "env": {
                 //         "node": true
                 //     },
                 //     "files": [
                 //         ".eslintrc.{js,cjs}"
                 //     ],
                 //     "parserOptions": {
                 //         "sourceType": "script"
                 //     }
                 // }
             ],
             
             // 指定解析器选项
             "parserOptions": {
                 "ecmaVersion": "latest",    // 校验ECMA最新版本
                 // 指定解析器：解析器
                 // Esprima 默认解析器
                 // Babel-ESLint babel解析器
                 "parser": "@typescript-eslint/parser",  
                 "sourceType": "module"  // 设置为"script"（默认），或者"module"代码百ECMAScript模块中
             },
             // ESLint支持使用第三方插件，在使用之前，必须使用npm安装它
             // 该eslint-plugin-前缀可以从插件名称被省略
             "plugins": [
                 "@typescript-eslint",
                 "vue"
             ],
             // eslint规则
             "rules": {
             }
         }
         
         ```
       
       * 安装指令
       
         ```bash
         cnpm install -D eslint-plugin-import eslint-plugin-vue eslint-plugin-node eslint-plugin-prettier eslint-config-prettier eslint-plugin-node @babel/eslint-parser
         ```
       
       * ```js
         module.exports = {
             "env": {
                 "browser": true,
                 "es2021": true,
                 "node": true,
                 "jest": true
             },
             // 规则继承
             "extends": [
                 // 全部规则默认是关闭的，这个配置开启推荐规则。
                 // 比如函数不能重名，对象不能出现重复key
                 "eslint:recommended",
                 // ts语法规则
                 "plugin:@typescript-eslint/recommended",
                 // vue3语法规则
                 "plugin:vue/vue3-essential",
                 // "plugin:prettier/recommended"
             ],
             // 要为特定类型的文件指定处理器
             "overrides": [
                 // {
                 //     "env": {
                 //         "node": true
                 //     },
                 //     "files": [
                 //         ".eslintrc.{js,cjs}"
                 //     ],
                 //     "parserOptions": {
                 //         "sourceType": "script"
                 //     }
                 // }
             ],
             
             // 指定解析器选项
             "parserOptions": {
                 "ecmaVersion": "latest",    // 校验ECMA最新版本
                 // 指定解析器：解析器
                 // Esprima 默认解析器
                 // Babel-ESLint babel解析器
                 "parser": "@typescript-eslint/parser",  
                 "sourceType": "module"  // 设置为"script"（默认），或者"module"代码百ECMAScript模块中
             },
             // ESLint支持使用第三方插件，在使用之前，必须使用npm安装它
             // 该eslint-plugin-前缀可以从插件名称被省略
             "plugins": [
                 "@typescript-eslint",
                 "vue"
             ],
             // eslint规则
             /**
              * "off"或者 0       ==> 关闭规则
              * "warn"或者 1     ==> 打开的规则作为警告（不影响代码执行）
              * "error"或者 2    ==> 规则作为一个错误（代码不能执行，界面报错）
              */
             "rules": {
                 // eslint(https://eslint.org/docs/latest/use/configure/rules)、
                 'no-var': 'error',
                 'no-multiple-empty-lines': ['warn', {max: 1}], // 不允许有多个空行
                 'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
                 'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
                 'no-unused-vars': 'off',
                 // typeScript(https://typescript-eslint.io/rules/)
                 '@typescript-eslint/no-unused-vars': 'error', // 禁止定义未使用的变量
                 '@typescript-eslint/prefer-ts-expect-error': 'error', // 禁止使用@ts-ignore
                 '@typescript-eslint/no-explicit-any': 'off', // 禁止使用any类型
                 '@typescript-eslint/no-non-null-assertion': 'off',
                 '@typescript-eslint/no-namespace': 'off',  // 禁止使用命名空间
                 '@typescript-eslint/no-empty-function': 'off',
                 '@typescript-eslint/semi': 'off',
         
                 // eslint-plugin-vue (https://eslint.vuejs.org/rules/)
                 'vue/multi-word-component-names': 'off', // 要求组件名称始终为 “_”链接的单词
                 'vue/script-setup-uses-vars': 'error', // 确保在 <script setup> 中使用的所有变量都被定义
                 'vue/no-mutating-props': 'error', // 禁止在 props 上使用 v-model
                 'vue/attribute-hyphenation': 'off', // 对模板中的自定义组件强制执行属性命名样式
             }
         }
         
         
         ```
       
       * ```bash
         cnpm install -D eslint-plugin-prettier prettier eslint-config-prettier
         ```
       
       * ```json
         {
             "singleQuote": true, // 单引号
             "semi": false, // 分号不要
             "bracketSpacing": true, 
             "htmlWhitespaceSensitivity": "ignore",
             "endOfLine": "auto",
             "trailingComma": "all",
             "tabWidth": 2 // 缩进2个空格
         }
         ```
       
       * prettierignore忽略文件
       
         ```
         /dist/*
         /html/*
         .local
         /node-modules/**
         **/*.sh
         /public/*
         ```
       
         

2. xxxx

3. xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
