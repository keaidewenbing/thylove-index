module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true,
        "jest": true
    },
    // 规则继承
    "extends": [
        // 全部规则默认是关闭的，这个配置开启推荐规则。
        // 比如函数不能重名，对象不能出现重复key
        "eslint:recommended",
        // ts语法规则
        "plugin:@typescript-eslint/recommended",
        // vue3语法规则
        "plugin:vue/vue3-essential",
        // "plugin:prettier/recommended"
    ],
    // 要为特定类型的文件指定处理器
    "overrides": [
        // {
        //     "env": {
        //         "node": true
        //     },
        //     "files": [
        //         ".eslintrc.{js,cjs}"
        //     ],
        //     "parserOptions": {
        //         "sourceType": "script"
        //     }
        // }
    ],
    
    // 指定解析器选项
    "parserOptions": {
        "ecmaVersion": "latest",    // 校验ECMA最新版本
        // 指定解析器：解析器
        // Esprima 默认解析器
        // Babel-ESLint babel解析器
        "parser": "@typescript-eslint/parser",  
        "sourceType": "module"  // 设置为"script"（默认），或者"module"代码百ECMAScript模块中
    },
    // ESLint支持使用第三方插件，在使用之前，必须使用npm安装它
    // 该eslint-plugin-前缀可以从插件名称被省略
    "plugins": [
        "@typescript-eslint",
        "vue"
    ],
    // eslint规则
    /**
     * "off"或者 0       ==> 关闭规则
     * "warn"或者 1     ==> 打开的规则作为警告（不影响代码执行）
     * "error"或者 2    ==> 规则作为一个错误（代码不能执行，界面报错）
     */
    "rules": {
        // eslint(https://eslint.org/docs/latest/use/configure/rules)、
        'no-var': 'error',
        'no-multiple-empty-lines': ['warn', {max: 1}], // 不允许有多个空行
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-unused-vars': 'off',
        // typeScript(https://typescript-eslint.io/rules/)
        '@typescript-eslint/no-unused-vars': 'error', // 禁止定义未使用的变量
        '@typescript-eslint/prefer-ts-expect-error': 'error', // 禁止使用@ts-ignore
        '@typescript-eslint/no-explicit-any': 'off', // 禁止使用any类型
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/no-namespace': 'off',  // 禁止使用命名空间
        '@typescript-eslint/no-empty-function': 'off',
        '@typescript-eslint/semi': 'off',

        // eslint-plugin-vue (https://eslint.vuejs.org/rules/)
        'vue/multi-word-component-names': 'off', // 要求组件名称始终为 “_”链接的单词
        'vue/script-setup-uses-vars': 'error', // 确保在 <script setup> 中使用的所有变量都被定义
        'vue/no-mutating-props': 'error', // 禁止在 props 上使用 v-model
        'vue/attribute-hyphenation': 'off', // 对模板中的自定义组件强制执行属性命名样式
    }
}
/*
*
* //    "eslint-config-prettier": "^9.1.0",
//    "eslint-plugin-import": "^2.29.1",
//    "eslint-plugin-node": "^11.1.0",
//    "eslint-plugin-prettier": "^5.1.3",
//    "eslint-plugin-vue": "^9.24.0",
* //    "@babel/eslint-parser": "^7.24.1",
//    "@typescript-eslint/eslint-plugin": "latest",
* //    "@typescript-eslint/parser": "^7.5.0",
*
* */
