import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
// target: 'http://127.0.0.1:8090',

//vue3+vite+ts 配置@时vscode报找不到__dirname的问题：
//报错原因：path 模块是 node.js 的内置模块，而 node.js 默认不支持 ts 文件的
//解决：安装 @type/node 依赖包 npm install @types/node --save-dev 或 cnpm i @types/node --save-dev、pnpm i @types/node --save-dev
import path from 'path'

export default defineConfig({
    plugins: [vue()],
    base: './',
    server: {
        port: 8899,
        cors: true,
        proxy: {
            '/api': {
                // target: 'http://asyou.top:8090',
                target: 'http://127.0.0.1:8091',
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, '')
            },
        }
    },
    // 文件夹的别名
    resolve: {
        alias: [
            {
                find: '@',
                replacement: path.resolve(__dirname, 'src')
            },
            {
                find: 'pub',
                replacement: path.resolve(__dirname, 'public/')
            },
            {
                find: 'comps',
                replacement: path.resolve(__dirname, 'src/components')
            },
            {
                find: 'apis',
                replacement: path.resolve(__dirname, 'src/apis')
            },
            {
                find: 'views',
                replacement: path.resolve(__dirname, 'src/views')
            },
            {
                find: 'routes',
                replacement: path.resolve(__dirname, 'src/router')
            },
            {
                find: 'store',
                replacement: path.resolve(__dirname, 'src/store')
            },
            {
                find: 'utils',
                replacement: path.resolve(__dirname, 'src/utils')
            },
            {
                find: 'styles',
                replacement: path.resolve(__dirname, 'src/styles')
            },
            {
                find: 'layout',
                replacement: path.resolve(__dirname, 'src/Layout')
            },
            {
                find: 'models',
                replacement: path.resolve(__dirname, 'src/models')
            },
            {
                find: 'hooks',
                replacement: path.resolve(__dirname, 'src/hooks')
            }
        ]
    }

})
