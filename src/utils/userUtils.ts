import router from "@/router";
import { logout as logoutApi } from "@/request/api/user.ts";

export const logout = () => {
    logoutApi().then(res => {
        console.log(res)
        if(res.data.code === 200) {
            sessionStorage.clear()
            router.push({path: '/', force: true})
        }
    })
}
