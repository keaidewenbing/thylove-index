import {ElMessage} from "element-plus";
import {existsUsername} from "@/request/api/user.ts";
export const validatePhoneNumber = (phoneNumber: string) => {
    let re = /^(?:(?:\+|00)86)?1[3-9]\d{9}$/;
    if (!re.test(phoneNumber)) {
        ElMessage.error("手机号码格式不正确！");
        return false;
    }
    return true;
}
// 手机号验证规则
export const rulePhoneNumber = (rule: any, value: any, callback: any) => {
    (rule === null)
    let re = /^(?:(?:\+|00)86)?1[3-9]\d{9}$/;
    if(value === '') {
        callback(new Error('请输入手机号！'))
    }else if (!re.test(value)) {
        callback(new Error('请输入正确的手机号！'))
    } else {
        callback()
    }
}
export const ruleUsername = (rule: any, value: any, callback: any) => {
    (rule === null)
    if(value !== '') {
        if(value.length < 3 || value.length > 20) {
            callback(new Error('用户名长度在3-20个字符之间！'))
        } else {
            existsUsername(value).then(res => {
                res.data.data ? callback() : callback(new Error('用户名已存在！'))
            })
        }
    } else {
        callback(new Error('用户名不能为空！'))
    }

}
export const selectRole = (rule: any, value: any, callback: any) => {
    (rule === null)
    if(value === '') {
        callback(new Error('请选择一个角色！'))
    }else {
        callback()
    }
}
// 邮箱验证规则
export const ruleEmail = (rule: any, value: any, callback: any) => {
    (rule === null)
    let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(value === '') {
        callback(new Error('请输入邮箱！'))
    }else if (!re.test(value)) {
        callback(new Error('请输入正确的邮箱！'))
    } else {
        callback()
    }
}

// 判断用户名是否存在
export const judgeUsername = (username:string) => {
    existsUsername(username).then(res => {
        return  res.data.data
    })
}






