import {getMenuList} from "@/request/api/menu.ts";
import {Base64} from "js-base64";
import {getDictList} from "@/request/api/dict.ts";
import userStore from "@/store/user";
import {getUserInfo} from "@/request/api/user.ts";

/**
 * 登录之后的一些操作，获取菜单、
 */
export const loginBefore = async () => {
    // 设置用户信息
    const store = userStore();
    const dataForDb = await getUserInfo();
    const user = dataForDb.data.data
    sessionStorage.setItem("username", Base64.encode(user.username))
    store.setUserInfo(user)
    // 获取菜单
    setMenuList();
    // 获取系统配置
    const resDict = await getDictList({pageNum: -1, condition: {name: ""}});
    sessionStorage.setItem("dictList", Base64.encode(JSON.stringify(resDict.data.data)))
}
export const setMenuList = async () => {
    sessionStorage.removeItem("menuList")
    const resMenu = await getMenuList({name: ""});
    sessionStorage.setItem("menuList", Base64.encode(JSON.stringify(resMenu.data.data)))
}
