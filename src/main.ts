import { createApp } from 'vue'
import App from '@/App.vue'
// 引入全局组件--顶部、底部都是全局组件
// import ThyloveTop from '@/components/thylove-top/index.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import "@/assets/fonts/iconfont.css"
import '@/style/reset.scss'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import router from "./router/index";
import {createPinia} from "pinia";
import store from "@/store";
const app = createApp(App)

// 全局注册el-icon
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
// 1.createPinia()、2.store有先后顺序
app.use(createPinia())
app.use(store)
app.use(ElementPlus)
app.use(router)
app.mount('#app')

