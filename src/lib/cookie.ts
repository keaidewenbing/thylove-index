const cookie = {
    /**
     * @name: 设置cookie值
     * @author: dsq
     * @date: 2023-09-05
     * @param:  cookieName   string  cookie名称
     * @param:  cookieValue  any cookie值
     * @param:  expireDay  number  cookie保存天数
     */
    setCookie(cookieName:string, cookieValue:string, expireDay = 720) {
        const d = new Date();
        d.setTime(d.getTime() + (expireDay * 24 * 60 * 60 * 1000));
        const expires = "expires=" + d.toUTCString();
        document.cookie = cookieName + "=" + cookieValue + "; " + expires;
    },
    /**
     * @name: 获取cookie值
     * @author: dsq
     * @date: 2023-09-05
     */
    getCookie(cookieName:string) {
        const name = cookieName + "=";
        const ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1);
            if (c.indexOf(name) !== -1) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    /**
     * @name: 清除cookie值
     * @author: dsq
     * @date: 2023-09-05
     * @param:  cookieName   string  cookie名称
     */
    clearCookie(cookieName:string) {
        const d = new Date();
        d.setTime(-1);
        const expires = "expires=" + d.toUTCString();
        document.cookie = cookieName + "=''; " + expires;
    },
    getToken() {
        const cookieName = "token";
        return this.getCookie(cookieName)
    },
    clearToken() {
        const cookieName = "token";
        this.clearCookie(cookieName)
    }
}
export default cookie;
