import {Ref} from "vue";

// User
export interface UserInter {
    id: number,
    nickName: string | null,
    phoneNumber: string | null,
    sex: string | null,
    status: string | null,
    avatar: string | null,
    userType: string | null,
    email: string | null
}

export interface SaveUserRuleForm {
    username: string
    sex: string
    phoneNumber: string
    email: string
    roleId?: string
    status?: boolean | string
}

export interface PasswdRuleForm {
    oldPasswd: string
    newPasswd: string
    confirmPasswd: string
}

// Menu
export interface MenuInter {
    id: string
    menuName: string
    title: string
    icon: string
    path: string
    component: string
    visible: string | boolean
    type: string | number
    orderNum: string
    perms: string
    children: Ref<Array<MenuInter>>
}

export interface SavRoleRuleForm {
    name: string
    roleKey: string
    status: boolean | string
    remark: string
    menuIds: Array<string>
}

// Role
export interface RoleInter {
    name: string
    id: number
}

/*
* 菜单的接口
* */
export interface SaveMenuRuleForm {
    parentId: string
    menuName: string
    title: string
    icon: string
    path: string
    component: string
    type: string | number
    orderNum: string
    perms: string
}

export interface ParentMenu {
    value: string
    label: string
    children: Ref<Array<ParentMenu>>
}

/**
 * 字典的接口
 */
export interface SaveDictRuleForm {
    name: string
    value: string
    remark: string
}

export interface DictInter {
    name: string
    value: string
    remark: string
}

/**
 * 弹框
 */
export interface DialogInter {
    dialogOverflowVisible: boolean
    fullscreen: boolean
    tips: string
    labelWith: string
}

export interface FormInter {
    label: string
    value: string
    props: string
    type?: string
    placeholder?: string
    dicData?: Array<any>,
    rules: Array<any>
}

export interface SaveProcessClassificationRuleForm {
    name:String
    code:String
    shortName:String
    companyId:String
    frontShow: Boolean
    remark:String
}
export interface ProcessClassificationInter {
    name:String
    code:String
    shortName:String
    companyId:String
    frontShow: Boolean
    remark:String
}
export interface GeneratorInterface {
    tableName: string,
    engine: string,
    tableComment: string,
    createTime: string
}

export interface VideoInterface {
    id: string,
    name: string,
    url:string,
    type: string,
    cover: string
}

export interface FormInter {
    label: string
    value: string
    prop: string
    type?: string
    placeholder?: string
    dicData?: Array<any>,
    rules: Array<any>
}
export interface videoDirInterface {
    parentId?: string
    name: string
    id?: string
    children?: Array<videoDirInterface>
    cover?: string
}
export interface PoetryInterface {
    id?: string
    name: string
    content: string
}
export interface SaveVideoDirRuleForm {
    parentId?: string
    name: string
    orderNo: number
    cover?: string
    type: number
}



