import axios from "@/request/http.ts"
import {SaveProcessClassificationRuleForm} from "@/ts/interface.ts";

const prefix = 'processEngine/processClassification/'
export const apiUrl = {
    list: `${prefix}` + 'list',
    save: `${prefix}` + 'save',
    delete: `${prefix}` + 'batchDelete',
    processClassificationInfo: `${prefix}` + 'getProcessClassificationInfo',
    alert: `${prefix}` + 'alert',

}
/**
 * 获取流程分类表列表
 * @param params
 */
export const getProcessClassificationList = (params: any) => axios.post(apiUrl.list, params)
/**
 * 添加流程分类表
 * @param params
 */
export const insertProcessClassification = (params: SaveProcessClassificationRuleForm) => axios.post(apiUrl.save, params)
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params: any) => axios.post(apiUrl.delete, params)

/**
 * 获取流程分类表信息
 * @param params
 */
export const getProcessClassificationInfo = () => axios.get(apiUrl.processClassificationInfo,)

/**
 * 修改流程分类表信息
 * @param params
 */
export const alertProcessClassification = (params: SaveProcessClassificationRuleForm) => axios.post(apiUrl.alert, params)





