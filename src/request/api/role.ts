import axios from "@/request/http.ts"
import {SavRoleRuleForm} from "@/ts/interface.ts";
const prefix = '/role'
export const apiUrl = {
    page: `${prefix}` + '/page',
    save: `${prefix}` + '/save',
    forRoleAddMenu: `${prefix}` + '/forRoleAddMenu',
}

interface RoleAddMenu {
    menuIds: Array<string>,
    roleId: string
}

export const getRoleList = (params:any) =>  axios.post(apiUrl.page, params)

export const insertRole = (params:SavRoleRuleForm) => axios.post(apiUrl.save, params)

export const forRoleAddMenu = (params:RoleAddMenu) => axios.post(apiUrl.forRoleAddMenu, params)
