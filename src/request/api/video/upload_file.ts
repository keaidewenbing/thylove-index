import axios from "../../http.ts"
const prefix = '/file/'
export const apiUrl = {
    upload: `${prefix}` + 'upload',
    upload_cover: `${prefix}` + 'uploadCover',
    del: `${prefix}` + 'deleteFile',
    delete: `${prefix}` + 'deleteFile',
    getPresignedObjectUrl: `${prefix}` + 'getPresignedObjectUrl',
}
const config = {
    headers: {
        'Content-Type': 'multipart/form-data',
    },
    timeout: 1000 * 60 * 15
}
export const file_upload = (params:FormData) =>  axios.post(apiUrl.upload, params, config)
export const file_upload_cover = (params:FormData) =>  axios.post(apiUrl.upload_cover, params, )
export const file_del = (params:{id: string|undefined}) =>  axios.get(apiUrl.del +"?id="+params.id)
export const batchDelete = (params:any) =>  axios.post(apiUrl.delete, params)

export const getPreviewUrl = (params:{objectName: string}) =>  axios.get(apiUrl.getPresignedObjectUrl +"?objectName="+params.objectName)