import axios from "../../http.ts"
const prefix = '/video_dir/'
export const apiUrl = {
    list: `${prefix}` + 'listV1',
    add: `${prefix}` + 'add',
    del: `${prefix}` + 'del',
    update: `${prefix}` + 'update',
    info: `${prefix}` + 'info',
}
export const getVideoDirList = (param: {name: string}) =>  axios.get(apiUrl.list+"?name" + param.name)
export const insertVideoDir = (param:any) =>  axios.post(apiUrl.add, param)
