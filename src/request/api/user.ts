import axios from "@/request/http.ts"
import {PasswdRuleForm, SaveUserRuleForm} from "@/ts/interface.ts";
const prefix = '/user/'
export const apiUrl = {
    page: `${prefix}` + 'page',
    save: `${prefix}` + 'save',
    delete: `${prefix}` + 'batchDelete',
    userInfo: `${prefix}` + 'getUserInfo',
    alert: `${prefix}` + 'alert',
    exitUsername: `${prefix}` + 'existsUsername',
    checkPasswd: `${prefix}` + 'checkPasswd',
    alertPassword: `${prefix}` + 'alertPasswd',
    logout: `${prefix}` + 'logout',

}
/**
 * 获取用户列表
 * @param params
 */
export const getUserList = (params:any) =>  axios.post(apiUrl.page, params)
/**
 * 添加用户
 * @param params
 */
export const insertUser = (params:SaveUserRuleForm) =>  axios.post(apiUrl.save, params)
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params:any) =>  axios.post(apiUrl.delete, params)

/**
 * 获取用户信息
 * @param params
 */
export const getUserInfo = () =>  axios.get(apiUrl.userInfo, )

/**
 * 修改用户信息
 * @param params
 */
export const alertUser = (params:SaveUserRuleForm) =>  axios.post(apiUrl.alert, params)

/**
 * 判断用户名是否存在
 * @param params
 */
export const existsUsername = (params:string) =>  axios.get(apiUrl.exitUsername + '/' + params, )

/**
 * 判断密码是否正确
 * @param params
 */
export const checkPassword = (params:string) =>  axios.get(apiUrl.checkPasswd + '/' + params, )

/**
 * 修改密码
 * @param params
 */
export const alertPassword = (params:PasswdRuleForm) =>  axios.get(apiUrl.alertPassword + `?oldPasswd=${params.oldPasswd}&newPasswd=${params.newPasswd}`)

/**
 * 退出
 */
export const logout = () => axios.post(apiUrl.logout)
