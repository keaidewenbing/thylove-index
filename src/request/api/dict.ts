import axios from "@/request/http.ts"
import {SaveDictRuleForm} from "@/ts/interface.ts";
const prefix = '/thyloveDict/'
export const apiUrl = {
    page: `${prefix}` + 'page',
    save: `${prefix}` + 'save',
    delete: `${prefix}` + 'batchDelete',
}
/**
 * 获取用户列表
 * @param params
 */
export const getDictList = (params:any) =>  axios.post(apiUrl.page, params)
/**
 * 添加用户
 * @param params
 */
export const insertDict = (params:SaveDictRuleForm) =>  axios.post(apiUrl.save, params)
/**
 * 批量删除
 * @param params
 */
export const batchDelete = (params:any) =>  axios.post(apiUrl.delete, params)
