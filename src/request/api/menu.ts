import axios from "@/request/http.ts"
import {SaveMenuRuleForm} from "@/ts/interface.ts";
const prefix = '/menu'
export const apiUrl = {
    list: `${prefix}` + '/list',
    save: `${prefix}` + '/save',
    getParentMenuSelect: `${prefix}` + '/getParentMenuSelect',
    getMenuIdList: `${prefix}` + '/getMenuIdList',
    getMenuSelect: `${prefix}` + '/getMenuSelect',
    getSecMenus: `${prefix}` + '/get1SecMenus',
    delete: `${prefix}` + '/delete',
}
export const getMenuList = (params:any) =>  axios.get(apiUrl.list + "?name=" + params.name)
export const insertMenu = (params:SaveMenuRuleForm) =>  axios.post(apiUrl.save, params)
export const getParenMenu = () =>  axios.get(apiUrl.getParentMenuSelect)
export const getMenuIdList = (roleId:string) => axios.get(apiUrl.getMenuIdList + "?roleId=" + roleId)
export const getMenuSelect = () => axios.get(apiUrl.getMenuSelect)
export const getSecMenus = () => axios.get(apiUrl.getSecMenus)


/**
 * 根据id删除菜单
 * @param params
 */
export const deleteMenu = (params: Array<string>) => axios.post(apiUrl.delete, params)
