import Axios, {AxiosResponse, InternalAxiosRequestConfig} from 'axios';
import {ElMessage} from 'element-plus'
export const baseUrl = import.meta.env.VITE_BASEURL;
const axios = Axios.create({
    baseURL: baseUrl,
    timeout: 30000,
});
export default axios

axios.interceptors.request.use(
    (config:InternalAxiosRequestConfig) => {
        // 重复请求处理 TODO
        // 添加token
        if(config.headers["Content-Type"] === '' || config.headers["Content-Type"] === undefined) {
            config.headers["Content-Type"] = "application/json; charset=utf-8";   // 默认类型
        }
        config.headers['Authorization'] = sessionStorage.getItem("token")
        // 如果请求登录接口
        const urls = [
            "/user/login",
            "/user/loginSMS",
            "/user/sendCheckCode",
        ]
        urls.forEach(item => {

            if(config.url != undefined && config.url.includes(item)){
                config.headers['Authorization'] = null
            }
        })
        // if (config.url === "/user/login" || config.url === "/user/loginSMS" || config.url === "/user/sendCheckCode") {
        //     config.headers['Authorization'] = null
        // }
        return config;
    },
    error => {
        ElMessage.error("请求出错了");
        console.log("请求出错url【" + error.config.url + "】")
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

const responseCode = new Map();
responseCode.set("code302", "接口重定向了！")
responseCode.set("code400", "参数不正确！")
responseCode.set("code401", "您未登录，或者登录已经超时，请先登录！")
responseCode.set("code403", "您没有权限操作！")
responseCode.set("code404", "请求地址出错！")
responseCode.set("code408", "请求超时！")
responseCode.set("code409", "系统已存在相同数据！")
responseCode.set("code500", "服务器内部错误！")
responseCode.set("code502", "服务未实现！")
responseCode.set("code502", "网关错误！")
responseCode.set("code503", "服务不可用！")
responseCode.set("code504", "服务暂时无法访问，请稍后再试！")
responseCode.set("code505", "HTTP版本不受支持！")
responseCode.set("code601", "验证码错误！")
responseCode.set("code602", "验证码已过期！")
responseCode.set("codeDefault", "异常问题，请联系管理员！")
// interface ResponseResult {
//     data: [],
//     msg: string,
//     status: number
// }
// 添加响应拦截器
axios.interceptors.response.use(
    (response:AxiosResponse):Promise<AxiosResponse> => {
        if(response.status !== 200 ) {
            if(responseCode.has("code"+ response.status)) {
                ElMessage.error(responseCode.get("code" + response.status))
            } else {
                ElMessage.error(responseCode.get("codeDefault"))
            }
            console.log("请求出错url【" + response.config.url + "】")
            // 响应出错
            return Promise.reject("出错了, 请稍后再试！");
        } else {
            // 登录接口
            if(response.config.url === "/user/login" || response.config.url === "/user/loginSMS") {
                saveToken(response.data.data.jwt)
            }
        }
        return Promise.resolve(response);
    },
    error => {
        ElMessage.error(error.message)
        console.log("请求出错url【" + error.config.url + "】")
        return Promise.reject(error);

    }
);

function saveToken(jwt:string) {
    if (!(jwt === '' || jwt === undefined)) {
        sessionStorage.setItem("token", jwt)
    }
}
