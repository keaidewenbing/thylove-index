import {defineStore} from "pinia";
// pinia仓库写法： 组合式API、选项式API写法

const useDetailStore = defineStore('Detail', {
    state: () => {
        return {
            count: 0,
            toke401: false,
            token: "",
            title: "security",
            againRequest: true,
            menuList: [

            ], // 菜单列表
            permList:[], // 权限列表
            // editableTabsValue: ['SysManage', 'SysTool'],
            editableTabsValue: sessionStorage.getItem("active") ? sessionStorage.getItem("active") : 'index',
            editableTabs: [{
                title: '首页',
                name: 'index',
            }],
            nav: [],
        }
    },
    actions: {
        increment () {
            this.count++
        },
        setToken401(val:boolean) {
            this.toke401 = val
        },

        setToken(token:string) {
            this.token = token
        },
        setTitle(title:string) {
            this.title = title
        },
        setEditableTabs(editableTabs:[{
            title: string,
            name: string,
        }]) {

            this.editableTabs = editableTabs
        },
        setEditableTabsValue(editableTabsValue:string) {
            sessionStorage.setItem("active", editableTabsValue)
            this.editableTabsValue = editableTabsValue
        },
        addTab(tab:{
            name: string,
            title: string
        }) {
            // 如果session中存在的话也要添加
            const index = this.editableTabs.findIndex(e=>e.name === tab.name)
            if(tab.name !== 'index'){
                if(sessionStorage.getItem("tab")) {
                    const tb = JSON.parse(decodeURIComponent(window.atob(sessionStorage.getItem("tab") || '')))
                    if(tab.name !== tb.name){
                        this.editableTabs = this.editableTabs.filter(t => t.name !== tb.name)
                        this.editableTabs.push(tb)
                    }
                }
                sessionStorage.setItem("tab", window.btoa(window.encodeURIComponent(JSON.stringify({
                    title: tab.title,
                    name: tab.name
                }))))
            }
            else {
                sessionStorage.removeItem("tab")
            }
            // 没有找到
            if(index === -1){
                this.editableTabs.push({
                    title: tab.title,
                    name: tab.name,
                });
            }
            // 切换标签
            sessionStorage.setItem("active", tab.name)
            this.editableTabsValue = tab.name;
        },
        setNav(menus:any[]) {
            this.nav = menus
        },
    },
    getters: {

    }
})

// 获取仓库的方法对外暴露
export default useDetailStore;

