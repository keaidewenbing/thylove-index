import {defineStore} from "pinia";

const userStore = defineStore('userStore',
    {
        state: () => {
            return {
                userInfo: {
                    username: 'admin',
                    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
                }
            }
        },
        actions: {
            setUserInfo(userInfo: any) {
                this.userInfo = userInfo
            }
        }
    }
);
export default userStore;
