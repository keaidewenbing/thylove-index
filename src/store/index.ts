import { createPinia } from 'pinia'
import piniaPluginPersist from 'pinia-plugin-persist'
// export default createPinia();
const store = createPinia();
store.use(piniaPluginPersist)
export default store;
