import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";
const routerHistory = createWebHistory()
import {Base64} from 'js-base64'
const routes:Readonly<RouteRecordRaw[]> = [
    {
        path: '/',
        name: 'login',
        component: () => import(`@/pages/sys/login/index.vue`),
    },
    {
        path: '/main',
        name: 'main',
        component: () => import(`@/pages/main/Main.vue`),
        meta: {
            requireAuth: true
        },
        children :[
            {
                name: 'index',
                path: '/index',
                component: () => import(`@/pages/main/Index.vue`),
                meta: {
                    requireAuth: true
                }
            },
            {
                name: 'SideMenu',
                path: '/sys/SideMenu',
                component: () => import(`@/components/SideMenu.vue`)
            },
            {
                name: 'userInfo',
                path: '/sys/userInfo',
                component: () => import(`@/pages/sys/userInfo/Index.vue`)
            },
        ]
    },
    {
        name: "refresh",
        path: `/refresh`,
        component: () => import('@/utils/refresh.vue')
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('@/components/404/404.vue'),
        meta: { title: '页面丢失了~' }
    },
]
const router = createRouter({
    history: routerHistory,
    routes,
    scrollBehavior: () => ({ left: 0, top: 0 }),
})

const mainRoute = routes.filter(value => value.path === '/main')[0]
function addDynamicRoute(menuList:any[]) {
    const viteModules = import.meta.glob('@/pages/**/*.vue');
    const regex = /^\/src\/pages\/.*\/[i|I]ndex\.vue$/;
    const map = new Map();
    for (const module in viteModules) {
        if(regex.test(module)) {
            map.set(module.replace('/src/pages/','').replace('/index.vue', '').replace('/Index.vue', '').toLocaleLowerCase(), module)
        }
    }
    if(menuList != undefined) {
        menuList.forEach((item:any) => {
            for (const childrenKey in item.children) {
                const t = item.children[childrenKey]
                let key = t.component.toLocaleLowerCase().replace(" ", "");
                let component = map.get(key);
                mainRoute.children?.push({
                    name: t.menuName,
                    path: t.path,
                    component: () => import(/* @vite-ignore */component)
                })
            }
        })
    }
    return mainRoute;
}
const setMenuList = async (menuList:any) => {
    const dynamicRoute = addDynamicRoute(menuList)
    router.addRoute(dynamicRoute)
    console.log(dynamicRoute)

}
const menuListBase = sessionStorage.getItem("menuList")
if(menuListBase != null) {
    setMenuList(JSON.parse(Base64.decode(menuListBase)))
}
router.beforeEach(async(to, from, next) => {
    // 判断该路由是否需要登录权限
    if (sessionStorage.token && to.matched.some(record => record.meta.requireAuth)){
        next();
    } else if(to.matched.some(record => record.meta.requireAuth)) {
        // 去登录
        next('/');
    } else {
        next();
    }
})
export default router;
