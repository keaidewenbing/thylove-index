FROM nginx:latest
MAINTAINER dsq

#声明容器内主进程所对应的端口
# EXPOSE 8095
RUN rm -rf /etc/nginx/nginx.conf
COPY thylove-index.nginx.conf  /etc/nginx/nginx.conf

COPY dist  /usr/share/nginx/html/
CMD ["nginx", "-g", "daemon off;"]